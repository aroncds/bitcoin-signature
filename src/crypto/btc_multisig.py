# -*- coding: utf-8 -*-
from bitcoin.core import CMutableTransaction, x
from bitcoin.wallet import CBitcoinAddress, CBitcoinSecret
from bitcoin.core.script import CScript, SignatureHash, SIGHASH_ALL, OP_CHECKMULTISIG
from bitcoin.core.scripteval import VerifyScript, SCRIPT_VERIFY_P2SH


def mk_address_p2sh(pub_keys=[]):
    pk_len = len(pub_keys)
    stack = [pk_len]
    stack.extend(pub_keys)
    stack.extend([pk_len, OP_CHECKMULTISIG])
    return CBitcoinAddress\
        .from_scriptPubKey(CScript(stack).to_p2sh_scriptPubKey())


def mk_sign_transaction(raw, private_key):
    tr = CMutableTransaction.deserialize(x(raw))
    #sighash = SignatureHash(txin_scriptPubKey, tx, 0, SIGHASH_ALL)
    #sign = private_key.sign(sighash)
    