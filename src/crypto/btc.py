# -*- coding: utf-8 -*-
from bitcoinlib.keys import HDKey
from bitcoinlib.wallets import HDWallet, wallet_delete_if_exists
from bitcoinlib.transactions import Transaction

NETWORK = "mainet"

# do the second sign
def btc_sign_transaction(key_machine, code):
    transaction = Transaction.import_raw(code, network=NETWORK)
    transaction.sign(key_machine)
    transaction.verify()
    return transaction.raw_hex()
