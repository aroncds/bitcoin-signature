# -*- coding: utf-8 -*-
import os
import argparse

from crypto.btc import bt_sign_transaction
from consts import crypto, errors

FILE_INPUT_DIR = "./in_dir"
FILE_OUTPUT_DIR = "./out_dir"


def read(dir):
    for name in os.listdir(dir):
        path = "{}{}".format(dir, name)
        yield (name, open(path, "rb").read()[:-1])

def save(dir, filename, data):
    with open("{}{}".format(dir, filename), 'w') as f:
        f.write(data)

# sign transactions
def mk_sign_transactions(currency, key, raw):
    if currency == crypto.Bitcoin:
        return bt_sign_transaction(key, raw)

    raise errors.NOT_SUPPORTED

def run_sign_transaction(key):
    for (name, transaction) in read(FILE_INPUT_DIR):
        save(FILE_OUTPUT_DIR, name,
             mk_sign_transactions(crypto.Bitcoin, key, transaction))


if __name__ == "__main__":
   parser = argparse.ArgumentParser(description='Sign transactions')
   parser.add_argument('--pkey', type=str, nargs=1, help='set private key to sign')
   args = parser.parse_args()

