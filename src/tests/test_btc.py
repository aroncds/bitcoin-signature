import crypto.btc

from bitcoinlib.keys import HDKey
from crypto.btc import bt_sign_transaction

NETWORK = "testnet"
crypto.btc.NETWORK = NETWORK

machine_key = HDKey("dd83fd69ad618a3a20515b587663cf8867d801aaeafe78ca4fbccebd1c4408a0", network=NETWORK)
user_key = HDKey("4a8c52af7726911831bfef0ae45c39c5bb2075d12629040779cd247efd05006f", network=NETWORK)

transaction = "0200000001a2c7f9a3a356194d6619e11b0724bbd857c380a84d5f38fc94822a8e5c48ecd60000000000ffffffff01a0bb0d000000000017a9144600c95c4d0b4af681aa804437f318cb8712c3af8700000000"

def test_make_multisig_transaction():
    bt_sign_transaction(machine_key, transaction)
