# -*- coding: utf-8 -*-

from crypto.btc_multisig import mk_address_p2sh, mk_sign_transaction

from bitcoin.core import x
from bitcoin import SelectParams
from bitcoin.wallet import P2PKHBitcoinAddress, CBitcoinAddress, CBitcoinSecret

SelectParams("testnet")

key_machine = CBitcoinSecret('cVVHVPrZ8EvkJqcDgAdUFFrkNEQTF5EzjYtTq7YmWQ4syTPoynHz')
keys = [
    CBitcoinAddress('2MydNEN3ExpqGwm1n1G6LRSBcJjj4n5Hrj3'),
    CBitcoinAddress('2N4xDHMgbSAib5acv29TmXDCZLvF7bBcCWg')
]

transaction = "0200000001926058b499fb026f343022d9355ac48d011bd5b1263ff1b45560076b6a758fde0000000000ffffffff01a0bb0d000000000017a9144600c95c4d0b4af681aa804437f318cb8712c3af8700000000"

def test_make_address():
    address_expected = '2MxA7ytbVgDYYhihTpMt5gHnMnv4dE94UU6'
    assert(address_expected == str(mk_address_p2sh(keys)))

def test_sign_transaction():
    assert(1 == str(key_machine.to_scriptPubKey()))
    mk_sign_transaction(transaction, key_machine)
